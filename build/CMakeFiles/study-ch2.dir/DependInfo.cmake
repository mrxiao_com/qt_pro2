# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/xz/Qt_study/Study/ch02/build/inc/moc_CpuWidget.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/inc/moc_CpuWidget.cpp.o"
  "/home/xz/Qt_study/Study/ch02/build/inc/moc_MainWindow.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/inc/moc_MainWindow.cpp.o"
  "/home/xz/Qt_study/Study/ch02/build/inc/moc_MemoryWidget.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/inc/moc_MemoryWidget.cpp.o"
  "/home/xz/Qt_study/Study/ch02/build/inc/moc_SysInfo.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/inc/moc_SysInfo.cpp.o"
  "/home/xz/Qt_study/Study/ch02/build/inc/moc_SysInfoLinuxImpl.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/inc/moc_SysInfoLinuxImpl.cpp.o"
  "/home/xz/Qt_study/Study/ch02/build/inc/moc_SysInfoWidget.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/inc/moc_SysInfoWidget.cpp.o"
  "/home/xz/Qt_study/Study/ch02/src/CpuWidget.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/src/CpuWidget.cpp.o"
  "/home/xz/Qt_study/Study/ch02/src/MainWindow.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/src/MainWindow.cpp.o"
  "/home/xz/Qt_study/Study/ch02/src/MemoryWidget.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/src/MemoryWidget.cpp.o"
  "/home/xz/Qt_study/Study/ch02/src/SysInfo.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/src/SysInfo.cpp.o"
  "/home/xz/Qt_study/Study/ch02/src/SysInfoLinuxImpl.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/src/SysInfoLinuxImpl.cpp.o"
  "/home/xz/Qt_study/Study/ch02/src/SysInfoWidget.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/src/SysInfoWidget.cpp.o"
  "/home/xz/Qt_study/Study/ch02/src/main.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/src/main.cpp.o"
  "/home/xz/Qt_study/Study/ch02/build/study-ch2_autogen/mocs_compilation.cpp" "/home/xz/Qt_study/Study/ch02/build/CMakeFiles/study-ch2.dir/study-ch2_autogen/mocs_compilation.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "Clang")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "QT_CHARTS_LIB"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NETWORK_LIB"
  "QT_QMLMODELS_LIB"
  "QT_QML_LIB"
  "QT_QUICK_LIB"
  "QT_SQL_LIB"
  "QT_WIDGETS_LIB"
  "QT_XML_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "../"
  "study-ch2_autogen/include"
  "../inc"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtCore"
  "/opt/Qt5.14.1/5.14.1/gcc_64/./mkspecs/linux-g++"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtWidgets"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtGui"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtQml"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtNetwork"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtQuick"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtQmlModels"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtSql"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtXml"
  "/opt/Qt5.14.1/5.14.1/gcc_64/include/QtCharts"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
